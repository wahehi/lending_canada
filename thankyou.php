<?php include('header.php'); ?>
<div class="banner">
	<img src="assets/img/bg4.png" class="">
	<div class="banner-content">
	<div class="container">
		<div class="row">
			<div class="col-md-offset-7 col-md-5">
				<div clss='bc-block'>
					<div class="bc-greeting">
						THANK YOU,
						<div>[FIRSTNAME]</div>
					</div>


					<div class="bc-intro">
						This is the beginning <br/>of a profitable relationship.
					</div>
				</div>
			</div>
		</div>
	</div>
		
	</div>
</div>

<section class="half-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="hs-border">
					<h1 class="title">What happens next?</h3>
						<div class="hs-content">
							<p>Your Relationship Manager will be calling soon to confirm a time for your free demo—so you can keep more of the money your practice earns without delay.</p>

							<p>
								Your <span class="text-blue"><strong>demo will start with a brief, guided tour of the LendingUSA portal.</strong></span> We’ll show you how easy it is to get a loan decision in seconds, and complete the entire loan process in just seven minutes. You can ask questions, test-drive the technology, kick the tires, and look under the hood.
							</p>

							<p>
								<span class="text-blue"><strong>Bring a monthly statement from your current financing company …</strong></span> and your Relationship Manager will show you how much you could save with NO COST patient financing from LendingUSA.*
							</p>

							<p>
								<span class="text-blue"><strong>We look forward to speaking with you soon.</strong></span><br />
								We’ll send a Starbucks gift card to say thanks upon your demo’s completion. 
							</p>

							<a href="#" class="btn btn-green btn-hs"><strong>LEARN MORE</strong> ABOUT NO-COST PATIENT FINANCING*</a>
							<a href="#" class="btn btn-green btn-hs">EXPLORE THE LENDING USA MERCHANT DASHBOARD</a>
						</div>


						<div class="clearfix"></div>
						<p class="hs-note">*Certain subprime loans will result in a lower payout percentage.</p>

				</div>
			</div>
			<div class="col-md-4">
				<div class="hs-sidebar">
					<h3>Questions?</h3>
					<div class="">
						<p>Starting today, your Relationship Manager will always be just a click or call away.</p>
						<a href="#" class="btn btn-green btn-hss">EMAIL</i></a>
						<span>or call <span class="text-blue"><strong>1-800-574-4996.</strong></span> </span>
					</div>

					<h3>Want to learn more?</h3>
					<div class="">
						<p>Our free monthly newsletter keeps you informed of important issues in patient financing and practice profitability.</p>

						<form class="subcriber-form">
							<input type="text" name="" class="subscriber-email" placeholder="Email">
							<button type="submit" class="btn btn-green">SUBSCRIBE</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="bg-gray">
	<div class="container">
		<h3 class="title text-center">Here’s what patients like yours say about LendingUSA …</h3>
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="rating-post" data-mh="rating-post-mh" style="height: 179px;">
					<div class="rating-box">
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
					</div>
					<div class="rating-title">Great</div>
					<div class="rating-message">Was easy and fast.</div>
					<div class="rating-meta">
						<span class="rating-user">Virginia,</span><span class="rating-day">Thursday</span>
						<div class="rating-date">February 11, 2016</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="rating-post" data-mh="rating-post-mh" style="height: 179px;">
					<div class="rating-box">
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
					</div>
					<div class="rating-title">Fast, excellent service</div>
					<div class="rating-message">Highly recommend.</div>
					<div class="rating-meta">
						<span class="rating-user">Erin Haase,</span><span class="rating-day">Wednesday</span>
						<div class="rating-date">February 10, 2016</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="rating-post" data-mh="rating-post-mh" style="height: 179px;">
					<div class="rating-box">
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
					</div>
					<div class="rating-title">Very pleased</div>
					<div class="rating-message">Very impressed on how quickly the loan was funded. It only took a few days. Thank you.</div>
					<div class="rating-meta">
						<span class="rating-user">Jaquelyn,</span><span class="rating-day">Wednesy</span>
						<div class="rating-date">February 10, 2016</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="rating-post border-none" data-mh="rating-post-mh" style="height: 179px;">
					<div class="rating-box">
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
						<i class="ico ico-star"></i>
					</div>

					<div class="rating-title">Very Helpful</div>
					<div class="rating-message">The contact was prompt, courteous and helpful. I needed the loan and you came through!</div>
					<div class="rating-meta">
						<span class="rating-user">Davis R,</span><span class="rating-day">Sunday</span>
						<div class="rating-date">January 17, 2016</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>
<?php include('footer.php'); ?>