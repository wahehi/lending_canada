
</div> <!-- cbody -->
	<div id="footer">
		<div class="container">
			<div class="row">
				<ul class="footer-links">
				<li class="bar"><a href="#">Privacy Policy</a></li>
				<li><a href="#">Terms of Use</a></li>
				<li><a href="#"><i class="ico ico-fb"></i> Facebook</a></li>
				<li><a href="#"><i class="ico ico-twitter"></i> Twitter</a></li>
				<li><a href="#"><i class="ico ico-linkedin"></i> LinkedIn</a></li>
				<li><a href="#"><i class="ico ico-gplus"></i> Google+</a></li>
			</ul>
			<div class="footer-info">
				All loans are made by Cross River Bank, a New Jersey State Chartered Bank. Member FDIC. Loan amounts range from $1,000 to $35,000. No loans are offered in Connecticut, New York, or Vermont. An origination fee of 8% is included in the principal loan amount. The Annual Percentage Rate (APR) is the cost of credit as a yearly rate. The APR offered to borrowers will depend on such factors as credit score, application information, loan amount, loan term, and credit history.
			</div>
			<div class="copyright">
				LendingUSA | 15303 Ventura Blvd., Suite 850 | Sherman Oaks, CA 91403  |  Copyright © 2016. LendingUSA, LLC. All Rights Reserved.
			</div>
			</div>
		</div>
	</div> <!-- footer -->
</div> <!-- master-wrapper-->
</body>
</html>