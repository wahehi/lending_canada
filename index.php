<?php include('header.php'); ?>

<div class="swiper-container">
    <div class="swiper-wrapper">
       <!--  <div class="swiper-slide">
        	<div class="content-block" class="" style="background-image: url(<?php echo base_url('assets/img/bg1.png'); ?>)">
			<div class="content-block-image">
				<img src="assets/img/bg1.png" class="content-block-img img-responsive">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-offset-7 col-md-5">
						<div class="content-block-caption">
							<h1 class="title">Faster, easier, <br><span class="text-red">more affordable</span> <br>financing starts here</h1>

							<div class="cbc-intro">
								<p>As a LendingUSA partner merchant, you’ll have a customized dashboard with all you need to manage patient financing throughout your practice, and complete loans in minutes at the point of sale.</p>

								<p>Best of all: it’s completely free.</p>
							</div>

							<a href="#" class="btn btn-green cbc-button">
								<div class=""><strong>ENROLL NOW</strong></div>
								<div class="">GET A CUSTOMIZED DASHBOARD FOR FREE*</div>
							</a>

							<div class="get-started">Get started with a virtual test-drive <div class="ico-triangle-down"></div></div>
						</div>
					</div>
				</div>
			</div>
			<a href="#" class="half-circle">
				<span>CLICK</span>
				<span>NUMBERED</span>
				<span>HOTSPOTS TO REVEAL</span>
				<span>KEY FEATURES</span>
				<div class="white-bar"></div>
				<span class="bottom-text">A few may really surprise you!</span>
				<i class="ico ico-arrow-down"></i>
			</a>
			</div>
        </div> -->
        <div class="swiper-slide">
        	<div class="content-block custom2" class="" style="background-image: url(<?php echo base_url('assets/img/bg5.png'); ?>)">
				<div class="hotspot-fader hide"></div> 

        		<div class="container">
					<div class="row">
						<div class="col-md-offset-1 col-md-11">
							<div class="hotspot-container">
								<img src="assets/img/hotspots.png" class="cb-image">
								<div class="cb-num cb-hotspot-1 pop-num" data-target="#pop1">1</div>
								<div class="cb-num cb-hotspot-2 pop-num" data-target="#pop2">2</div>
								<div class="cb-num cb-hotspot-3 pop-num" data-target="#pop3">3</div>
								<div class="cb-num cb-hotspot-4 pop-num" data-target="#pop4">4</div>
								<div class="cb-num cb-hotspot-5 pop-num" data-target="#pop5">5</div>
								<div class="cb-num cb-hotspot-6 pop-num" data-target="#pop6">6</div>
							</div>

							<div class="pop-container">
								<div id="pop1" class="pop-box hide">
									<div class="pc-wrap">
										<div class="pc-icon">
											<img src="assets/img/pop/pop_icon1.png">
										</div>
										<div class="pc-box">
											<div class="pc-title">
												<a href="#" class="text-red">
													Click here for <br />
													Seven-Minute Installment Loans
												</a>
											</div>

											<div class="pc-content">
												<p>LendingUSA turns less time into more money: </p>
												<ul class="money-list">
													<li>One click launches a simplified application</li>
													<li>Loan decisions take seconds</li>
													<li>Loans can be completed at the point of<br /> sale in as little as seven minutes</li>
												</ul>
											</div>
										</div>
									</div>
								</div> <!-- pop1 -->

								<div id="pop2" class="pop-box hide">
									<div class="pc-wrap">
										<div class="pc-icon">
											<img src="assets/img/pop/pop_icon2.png">
										</div>
										<div class="pc-box">
											<div class="pc-title">
												<a href="#" class="text-red">
													Get up to 30% more approvals
												</a>
											</div>

											<div class="pc-content">
												<p>Click here to track the progress of your <br />patients’ applications in real time.</p>
												
											</div>
										</div>
									</div>
								</div> <!-- pop2 -->

								<div id="pop3" class="pop-box hide">
									<div class="pc-wrap">
										<div class="pc-icon">
											<img src="assets/img/pop/pop_icon3.png">
										</div>
										<div class="pc-box">
											<div class="pc-title">
												<img src="assets/img/pop3_img.png" class="img-responsive pc-title-img">
											</div>

											<div class="pc-content">
												<p>Click here for a searchable archive<br /> of your applications. Plan for the <br /> future with data-based insight.</p>
												
											</div>
										</div>
									</div>
								</div> <!-- pop3 -->


								<div id="pop4" class="pop-box custom-popbox hide">
									<div class="pc-wrap">
										<div class="pc-wrap-img">
											<img src="assets/img/pop/pop_icon4.png">
										</div>
									</div>
								</div> <!-- pop4 -->

								<div id="pop5" class="pop-box hide">
									<div class="pc-wrap">
										<div class="pc-icon">
											<img src="assets/img/pop/pop_icon5.png">
										</div>
										<div class="pc-box">
											<div class="pc-title">
												<a href="#" class="text-red">
													Click here for <br />
													FREE marketing support
												</a>
											</div>

											<div class="pc-content">
												<p>Order printed brochures or download<br /> web banners. Link your site with the<br /> LendingUSA portal, and connect your<br /> patients with the money they need.</p>
												
											</div>
										</div>
									</div>
								</div> <!-- pop5 -->

								<div id="pop6" class="pop-box hide">
									<div class="pc-wrap">
										<div class="pc-icon">
											<img src="assets/img/pop/pop_icon6.png">
										</div>
										<div class="pc-box">
											<div class="pc-title">
												Watch this space for <br /> chart-topping growth
											</div>

											<div class="pc-content">
												<p>You’ll have a Relationship <br /> Manager (RM) to help you <br /> achieve it. He or she will <br />be a financing expert<br /> dedicated to your success.</p>
											</div>

											<a href="#" class="btn btn-green pop6-link">
												<div class="">MEET YOUR RM during</div>
												<div class=""><strong>YOUR FREE DEMO</strong></div>
											</a>
										</div>
									</div>
								</div> <!-- pop5 -->

							</div>

						</div>
					</div>
				</div>

			</div>

        </div>
    </div>
</div>


<!-- <div class="content-block" class="" style="background-image: url(<?php echo base_url('assets/img/bg1.png'); ?>)">
<div class="content-block-image">
	<img src="assets/img/bg1.png" class="content-block-img img-responsive">
</div>
<div class="container">
	<div class="row">
		<div class="col-md-offset-7 col-md-5">
			<div class="content-block-caption">
				<h1 class="title">Faster, easier, <br><span class="text-red">more affordable</span> <br>financing starts here</h1>

				<div class="cbc-intro">
					<p>As a LendingUSA partner merchant, you’ll have a customized dashboard with all you need to manage patient financing throughout your practice, and complete loans in minutes at the point of sale.</p>

					<p>Best of all: it’s completely free.</p>
				</div>

				<a href="#" class="btn btn-green cbc-button">
					<div class=""><strong>ENROLL NOW</strong></div>
					<div class="">GET A CUSTOMIZED DASHBOARD FOR FREE*</div>
				</a>

				<div class="get-started">Get started with a virtual test-drive <div class="ico-triangle-down"></div></div>
			</div>
		</div>
	</div>
</div>
<a href="#" class="half-circle">
	<span>CLICK</span>
	<span>NUMBERED</span>
	<span>HOTSPOTS TO REVEAL</span>
	<span>KEY FEATURES</span>
	<div class="white-bar"></div>
	<span class="bottom-text">A few may really surprise you!</span>
	<i class="ico ico-arrow-down"></i>
</a>
</div> -->
<?php include('footer.php'); ?>