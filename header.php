<?php
	function base_url($uri = '')
	{
		$_base_url = 'http://localhost/';

		if (isset($_SERVER['HTTP_HOST'])) {
			$base_url = (empty($_SERVER['HTTPS']) OR strtolower($_SERVER['HTTPS']) === 'off') ? 'http' : 'https';
			$base_url .= '://'. $_SERVER['HTTP_HOST'];
			$base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);
		}

		return $base_url . $uri;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Super Clothing Manufacturing</title>
	<?php include('metadata.php'); ?>
</head>
<body>

<div class="master-wrapper">
<div id="header">
	<div class="container">
		<a href="#" id="logo"><img src="assets/img/logo.png" class="img-responsive"></a>

		<div class="header-contanct">
			<div>CALL <strong>1-800-574-4996</strong></div>
			<a href="#" class="btn btn-green header-link">OR CLICK TO <strong>SCHEDULE A FREE DEMO</strong></a>
		</div>
	</div>
</div> <!--header -->

<div class="cbody">

