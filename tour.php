<?php include('header.php'); ?>

<div class="swiper-container">
    <div class="swiper-wrapper">
    	<div class="swiper-slide">
    		<div class="content-block" class="" style="background-image: url(<?php echo base_url('assets/img/bg2.png'); ?>)">
				<div class="content-block-image">
					<img src="assets/img/bg2.png" class="content-block-img img-responsive">
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-offset-7 col-md-5">
							<div class="content-block-caption">
								<h1 class="title text-center"><span class="text-red">NO COST* FINANCING</span><br/>in six easy steps</h1>

								<div class="cbc-intro">
									<p>At LendingUSA, we turn large cases into low monthly payments, with budget-friendly installment loans. Care becomes more affordable for patients, so more of them accept it. Getting paid becomes less of a hassle for providers like you. Production and income go up, expenses go down, the average practice saves up to $17,000 a year†—and we make it all easy.</p>

								</div>

								<a href="#" class="btn btn-green cbc-button">
									

									<div class="">SEE HOW QUICK & easy FINANCING CAN BE</div>
									<div class=""><strong>SCHEDULE A FREE DEMO</strong></div>
								</a>
								<br /><br /><br />
								<small class="text-gray">*Certain subprime loans will result in a lower payout percentage. †Compared to the industry leader.</small>
							</div>
						</div>
					</div>
				</div> 
    	</div>
    		<a href="#" class="half-circle">
				<span>FOLLOW</span>
				<span>A LOAN FROM</span>
				<span>APPLICATION TO</span>
				<span>FUNDING</span>
				<div class="white-bar"></div>
				<span class="bottom-text">A few may really surprise you!</span>
				<i class="ico ico-arrow-down"></i>
			</a>
    	</div>
    	<div class="swiper-slide">
    		<div class="content-block">
    			<div class="content-block-image">
					<img src="assets/img/step/step1.png" class="content-block-img img-responsive">
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-7 content-block-image1">
							<img src="assets/img/step/step1.png" class="">
						</div>
						<div class="col-md-5">
							<div class="content-block-caption">
							<h1 class="title">Step One: Click One</h1>

							<div class="cbc-intro">
									<p>You’ll have a <span class="text-red"><strong>streamlined dashboard</strong></span> with all you need to arrange a single loan—or manage dozens. Click once to start a new application. Come back anytime for real-time status updates.</p>
							</div>
							</div> <!--content-block-caption-->
						</div>
					</div>
				</div>

    		</div>
    	</div>

    	<div class="swiper-slide">
    		<div class="content-block">
    			<div class="content-block-image">
					<img src="assets/img/step/step2.png" class="content-block-img img-responsive">
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-7 content-block-image1">
							<img src="assets/img/step/step2.png" class="">
						</div>
						<div class="col-md-5">
							<div class="content-block-caption">
							<h1 class="title">No hard questions</h1>

							<div class="cbc-intro">
									<p>Our simplified loan request form can be completed on any computer, tablet, or smartphone. It allows patients to check their rate and payment without affecting their credit score. When you link your website to our portal, patients can request a loan from their home, your office or waiting room—or anywhere else.</p>
							</div>
							</div> <!--content-block-caption-->
						</div>
					</div>
				</div>

    		</div>
    	</div>

    	<div class="swiper-slide">
    		<div class="content-block">
    			<div class="content-block-image">
					<img src="assets/img/step/step3.png" class="content-block-img img-responsive">
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-7 content-block-image1">
							<img src="assets/img/step/step3.png" class="">
						</div>
						<div class="col-md-5">
							<div class="content-block-caption">
							<h1 class="title">Loan decisions <br />take seconds</h1>

							<div class="cbc-intro">
									<p>And the entire loan process can be completed in as little as seven minutes. Healthcare providers can schedule same-day treatment, merchants can close deals on the spot, patients aren’t kept waiting, and practices keep moving along.</p>
							</div>
							</div> <!--content-block-caption-->
						</div>
					</div>
				</div>

    		</div>
    	</div>

    	<div class="swiper-slide">
    		<div class="content-block">
    			<div class="content-block-image">
					<img src="assets/img/step/step4.png" class="content-block-img img-responsive">
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-7 content-block-image1">
							<img src="assets/img/step/step4.png" class="">
						</div>
						<div class="col-md-5">
							<div class="content-block-caption">
							<h1 class="title"><span class="text-red">0%</span> interest <br />rate financing‡ for <br /><span class="text-red">100%</span> of your patients </h1>

							<div class="cbc-intro">
									<p><span class="text-red">Your cost: $0*</span> Promotional financing is a costly option with some lenders. At LendingUSA, it’s included for free. All your approved patients will be offered the 0% interest rate‡ ZIPLoan,™ <span class="text-red"><u>at no extra cost to you ></u></span></p>
							</div>
							</div> <!--content-block-caption-->
						</div>
					</div>
				</div>

    		</div>
    	</div>

    	<div class="swiper-slide">
    		<div class="content-block">
    			<div class="content-block-image">
					<img src="assets/img/step/step5.png" class="content-block-img img-responsive">
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-7 content-block-image1">
							<img src="assets/img/step/step5.png" class="">
						</div>
						<div class="col-md-5">
							<div class="content-block-caption">
							<h1 class="title">Paperwork won’t <br>slow you down</h1>

							<div class="cbc-intro">
									<p>There’s no paper at all in most cases. Documents are signed and tracked digitally, identities are verified digitally, and you’re digitally notified when a patient’s e-signature is overdue.</p>
							</div>
							</div> <!--content-block-caption-->
						</div>
					</div>
				</div>

    		</div>
    	</div>

    	<div class="swiper-slide">
    		<div class="content-block">
    			<div class="content-block-image">
					<img src="assets/img/step/step6.png" class="content-block-img img-responsive">
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-7 content-block-image1">
							<img src="assets/img/step/step6.png" class="">
						</div>
						<div class="col-md-5">
							<div class="content-block-caption">
								<h1 class="title">Cash in your account<br/> in 1 to 3 business days</h1>

								<div class="cbc-intro">
										<p>There are no deductions or fees with prime loans.* Transfer to your account starts as soon as agreements are signed and documentation is complete, we handle billing and collections, and we pay for any defaults.</p>
								</div>

								<a href="#" class="btn btn-green cbc-button">
									

									<div class="">HAVE A QUESTIONS? WANT TO KNOW MORE?</div>
									<div class=""><strong>SCHEDULE A FREE DEMO</strong></div>
								</a>
								<br /><br /><br />
								<small class="text-gray">*Certain subprime loans will result in a lower payout percentage. †Compared to the industry leader.</small>
							</div> <!--content-block-caption-->
						</div>
					</div>
				</div>

    		</div>
    	</div>
    </div>
</div>

<!-- <div class="content-block" class="" style="background-image: url(<?php echo base_url('assets/img/bg2.png'); ?>)">
<div class="content-block-image">
	<img src="assets/img/bg2.png" class="content-block-img img-responsive">
</div>
<div class="container">
	<div class="row">
		<div class="col-md-offset-7 col-md-5">
			<div class="content-block-caption">
				<h1 class="title text-center"><span class="text-red">NO COST* FINANCING</span><br/>in six easy steps</h1>

				<div class="cbc-intro">
					<p>At LendingUSA, we turn large cases into low monthly payments, with budget-friendly installment loans. Care becomes more affordable for patients, so more of them accept it. Getting paid becomes less of a hassle for providers like you. Production and income go up, expenses go down, the average practice saves up to $17,000 a year†—and we make it all easy.</p>

				</div>

				<a href="#" class="btn btn-green cbc-button">
					

					<div class="">SEE HOW QUICK & easy FINANCING CAN BE</div>
					<div class=""><strong>SCHEDULE A FREE DEMO</strong></div>
				</a>
				<br /><br /><br />
				<small class="text-gray">*Certain subprime loans will result in a lower payout percentage. †Compared to the industry leader.</small>
			</div>
		</div>
	</div>
</div> -->

</div>
<?php include('footer.php'); ?>