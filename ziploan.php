<?php include('header.php'); ?>

<div class="swiper-container">
    <div class="swiper-wrapper">
    	<div class="swiper-slide">
    		<div class="content-block" class="" style="background-image: url(<?php echo base_url('assets/img/bg3.png'); ?>)">
				<div class="content-block-image">
					<img src="assets/img/bg3.png" class="content-block-img img-responsive">
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-offset-7 col-md-5">
							<div class="content-block-caption custom1">
								<h1 class="title text-center"><span class="text-red">The LendingUSA ZIPLoan™ <hr /></span><strong>BETTER</strong> for patients and <br/> <strong>BETTER</strong> for you</h1>

								<div class="cbc-intro">
									<p><span class="text-blue"><strong>The ZIPLoan is win/win financing,</strong></span> designed to do right by everyone. It’s offered to every approved patient, and it delivers what patients want most:</p>

									<ul class="lid-list">
										<li><span class="text-blue"><strong>0% Interest rate,</strong></span> when the pay early</li>
										<li><span class="text-blue"><strong>No penalty interest rate</strong></span> when they don't</li>
										<li><span class="text-blue"><strong>No prepayment penalties</strong></span> ever</li>
										<li><span class="text-blue"><strong>No Hassles</strong></span> whatever they choose</li>
										<li><span class="text-blue"><strong>It gives you more sales-closing power</strong></span> at no extra cost.</li>
										<li><span class="text-red">
											<strong>NO PROVIDERS FEES*</strong>
										</span></li>
									</ul>
								</div>
								<a href="#" class="btn btn-green cbc-button">
									<div class=""><strong>ENROLL NOW</strong></div>
									<div class="">OFFER THE ZIPLoan TO PATIENTS FOR FREE*</div>
								</a>
								<br /><br /><br />
								<small class="text-gray">*Certain subprime loans will result in a lower payout percentage. †Compared to the industry leader.</small>
								<br />
							</div>
						</div>
					</div>
				</div>

				<a href="#" class="half-circle short-text custom1">
					<span>FOLLOW</span>
					<span>THE MONEY</span>
					<div class="white-bar"></div>
					<span class="bottom-text">Scroll down to learn more</span>
					<i class="ico ico-arrow-down"></i>
				</a>
			</div>
    	</div> <!-- swiper-slide -->

    	<div class="swiper-slide">
    		<div class="content-block">
    			<div class="content-block-image">
					<img src="assets/img/step/step7.png" class="content-block-img img-responsive">
				</div>

				<div class="container">
					<div class="row">
						<div class="col-md-7 content-block-image1">
							<img src="assets/img/step/step7.png" class="">
						</div>
						<div class="col-md-5">
							<div class="content-block-caption custom1">
								<h1 class="title">
									<span class="text-red"><strong>ZERO IN COMMON</strong></span> <br />
									with other lenders’ promotions
								</h1>

								<div class="cbc-intro">
									<p>0% interest financing works like a jack-in-the-box with many of our competitors. 0% jumps to 25% or more when promotions end, leading to hundreds, or thousands, of dollars in unexpected costs for patients who miss the promotional window. Providers face hefty costs, too, in the form of promotional financing fees. <span class="text-blue">The ZIPLoan solves both problems at the same time</span></p>

									<ul class="lid-list">
										<li><span class="text-blue"><strong>The ZIPLoan is free for providers*</strong></span></li>
										<li><span class="text-blue"><strong>Safer for patients</strong></span></li>
										<li><span class="text-blue"><strong>And a better choice for your business</strong></span></li>
									</ul>
								</div>
								<a href="#" class="btn btn-green cbc-button">
									<div class=""><strong>ENROLL NOW</strong></div>
									<div class="">OFFER THE ZIPLoan TO PATIENTS FOR FREE*</div>
								</a>
								<br /><br /><br />
								<small class="text-gray">*Certain subprime loans will result in a lower payout percentage. †Compared to the industry leader.</small>
								<br />
							</div> <!--content-block-caption-->
						</div>
					</div>
				</div>

    		</div>
    	</div> <!-- swiper-slide -->

    </div> <!-- swiper-wrapper -->
</div>



<!-- <div class="content-block" class="" style="background-image: url(<?php echo base_url('assets/img/bg3.png'); ?>)">
<div class="content-block-image">
	<img src="assets/img/bg3.png" class="content-block-img img-responsive">
</div>
<div class="container">
	<div class="row">
		<div class="col-md-offset-7 col-md-5">
			<div class="content-block-caption custom1">
				<h1 class="title text-center"><span class="text-red">The LendingUSA ZIPLoan™ <hr /></span><strong>BETTER</strong> for patients and <br/> <strong>BETTER</strong> for you</h1>

				<div class="cbc-intro">
					<p><span class="text-blue"><strong>The ZIPLoan is win/win financing,</strong></span> designed to do right by everyone. It’s offered to every approved patient, and it delivers what patients want most:</p>

					<ul class="lid-list">
						<li><span class="text-blue"><strong>0% Interest rate,</strong></span> when the pay early</li>
						<li><span class="text-blue"><strong>No penalty interest rate</strong></span> when they don't</li>
						<li><span class="text-blue"><strong>No prepayment penalties</strong></span> ever</li>
						<li><span class="text-blue"><strong>No Hassles</strong></span> whatever they choose</li>
						<li><span class="text-blue"><strong>It gives you more sales-closing power</strong></span> at no extra cost.</li>
						<li><span class="text-red">
							<strong>NO PROVIDERS FEES*</strong>
						</span></li>
					</ul>
				</div>
				<a href="#" class="btn btn-green cbc-button">
					<div class=""><strong>ENROLL NOW</strong></div>
					<div class="">OFFER THE ZIPLoan TO PATIENTS FOR FREE*</div>
				</a>
				<br /><br /><br />
				<small class="text-gray">*Certain subprime loans will result in a lower payout percentage. †Compared to the industry leader.</small>
				<br />
			</div>
		</div>
	</div>
</div>

</div> -->
<?php include('footer.php'); ?>