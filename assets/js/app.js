$(function() {

    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        direction: 'vertical'
    });

    $('.half-circle').on('click', function() {
    	swiper.slideNext();
    	return false;
    });


    $('.pop-num').on('mouseover', function() {
        target_pop = $(this).attr('data-target');
        
    	$('.hotspot-fader').removeClass('hide');
        $(target_pop).removeClass('hide');
    });

    $('.hotspot-fader').on('click', function() {
    	$(this).addClass('hide');
        $('.pop-box:not(.hide)').addClass('hide');
    });

});