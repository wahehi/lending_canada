<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

<link href="assets/css/bootstrap.css" rel="stylesheet" />
<link href="assets/css/swiper.css" rel="stylesheet" />
<link href="assets/css/styles.css" rel="stylesheet" />

<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets/js/jquery.matchHeight.js"></script>
<script type="text/javascript" src="assets/js/swiper.min.js"></script>
<script type="text/javascript" src="assets/js/app.js"></script>

